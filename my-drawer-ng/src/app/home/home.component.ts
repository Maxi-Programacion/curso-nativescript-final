import { Component, OnInit } from "@angular/core";
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import * as app from "tns-core-modules/application";
import { isAndroid } from "tns-core-modules/platform"
import { Noticia } from "../models/noticia.model";
import { Store } from "@ngrx/store";
import { AppState } from "../app.module";
import * as camera from "nativescript-camera";
import { Image } from "tns-core-modules/ui/image";
import * as imageSourceModule from "tns-core-modules/image-source";
import * as SocialShare from "nativescript-social-share";

@Component({
    selector: "Home",
    templateUrl: "./home.component.html"
})
export class HomeComponent implements OnInit {
    android: string;
    leyendo: Array<Noticia> = [];

    constructor(
        private store: Store<AppState>
    ) {
        // Use the component constructor to inject providers.
    }

    ngOnInit(): void {
        if (isAndroid) {
            this.android = "Android exclusive";
        }

        this.store.select((state) => state.noticias.noticiasLeyendo)
            .subscribe((data) => {
                const ns = data;
                console.log("HOME - ns");
                console.log(ns);
                if (ns != null) {
                    ns.map((n) => this.leyendo.push(n));
                    console.log("HOME - leyendo");
                    console.log(this.leyendo);
                }
            })
    }

    onDrawerButtonTap(): void {
        const sideDrawer = <RadSideDrawer>app.getRootView();
        sideDrawer.showDrawer();
    }

    onButtonTap(): void {
        camera.requestPermissions().then(
            function success() {
                const options = { width: 300, height: 300, keepAspectRatio: false, saveToGallery: true };
                camera.takePicture(options).then((imageAsset) => {
                    console.log("Tamaño: " + imageAsset.options.width + "x" + imageAsset.options.height);
                    console.log("KeepAspectRatio: " + imageAsset.options.keepAspectRatio);
                    console.log("Foto guardada!");

                    imageSourceModule.fromAsset(imageAsset).then((imageSource) => {
                        SocialShare.shareImage(imageSource, "Asunto: compartido desde el curso!");
                    }).catch((err) => {
                        console.log("Error -> " + err.message);
                    });
                }).catch((err) => {
                    console.log("Error -> " + err.message);
                })
            },
            function failure() {
                console.log("Permiso de cámara no aceptador por el usuario.");
            }
        )
    }
}
